/**
 * Frontend JS file to manage socket events.
 */

window.onload = function () {
    var messages = [];
    var socket = io.connect('http://192.168.0.5:8080'); //Lenovo G480
    //var socket = io.connect('http://10.0.1.26:8080'); //Scarab
    var field = document.getElementById("field");
    var sendButton = document.getElementById("send");
    var content = document.getElementById("content");
    var name = document.getElementById("name");

    socket.on('message', function (data) {
        if (data.message) {
            messages.push(data);
            var html = '';
            for (var i = 0; i < messages.length; i++) {
                html += '<b>' + (messages[i].username ? messages[i].username : 'Server') + ': </b>';
                html += messages[i].message + '<br />';
            }
            content.innerHTML = html;
            content.scrollTop = content.scrollHeight;
        } else {
            console.log("Algo pasó:", data);
        }
    });

    sendButton.onclick = function () {
        if (name.value == "") {
            alert("Debe escribir su nombre para poder continuar");
        } else {
            var text = field.value;
            socket.emit('send', {message: text, username: name.value});
        }
    };
}
